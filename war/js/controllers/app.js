
var app = angular.module('app', []);

app.controller('LoginController',['$scope','$http','$window',function($scope, $http,$window) {

	$scope.submit = function() {

		console.log($scope.username + " "+ $scope.password);

		var req = {
				method: 'POST',
				url: 'http://localhost:8888/rest/login/',
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				data: {	"username" : $scope.username,
					"password" : $scope.password }
		}

		$http(req).then(function(response){
			if(response) {
				var credentials = {'username' : $scope.username, 'tokenId': response.tokenId }
				$window.localStorage.setItem('credentials', JSON.stringify(credentials) );
				$window.location.href = '/html/home.html';
			}
			else {
				alert("No response");
			}
		});

	};
}]);



app.controller('RegisterController',['$scope','$http','$window', function($scope, $http,$window) {

	$scope.submit= function() {
		
		var urlGeocode = "https://maps.googleapis.com/maps/api/geocode/json?address="+$scope.street+",+"+$scope.zipCode+"+"+$scope.city+",+Portugal&key=AIzaSyBSSejEKfyY0mbYMLipqM5Dt4C6B-baB4I";

		$http.get(urlGeocode).then(function(response){
			if(response) {

				var data = response.data;

				var req = {
						method: 'POST',
						url: 'http://localhost:8888/rest/register/',
						contentType: "application/json; charset=utf-8",
						crossDomain: true,
						data: {"username":$scope.username,
							"name":$scope.name,
							"email":$scope.email,
							"nif":$scope.nif,
							"cc":$scope.cc,
							"mobileNumber":$scope.mobileNumber,
							"telephoneNumber": $scope.telephoneNumber,
							"street":$scope.street,
							"city":$scope.city,
							"zipCode":$scope.zipCode,
							"password":$scope.password,
							"confirmation":$scope.confirmation,
							"lat": data.results[0].geometry.location.lat,
							"lng": data.results[0].geometry.location.lng
						}
				}

				$http(req).then(function(response){
					if(response) {
						$window.location.href = '/html/login.html';
					}
					else {
						alert("No response");
					}
				});

			}
			else {
				alert("No response");
			}
		});

	};

}]);


app.controller('HomeController',['$scope','$http','$window','$rootScope',function($scope, $http,$window,$rootScope){

	$scope.seeProfile = function(){
		$window.location.href = '/html/profile.html';
	}

	$scope.logOut = function(){
		var req = {
				method: 'POST',
				url: 'http://localhost:8888/rest/logout/'+JSON.parse($window.localStorage['credentials']).username,
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				data: {}
		}

		$http(req).then(function(response){
			if(response) {
				$window.localStorage.removeItem('credentials')
				$window.location.href = '/html/login.html';
			}
			else {
				alert("Error on server");
			}
		});

	}



}]);

app.controller('MapController', ['$scope','$rootScope',function($scope,$rootScope) {
	var map

	$scope.initialize = function() {
		map = new google.maps.Map(document.getElementById('map_div'), {
			center: {lat: 38.659784, lng: -9.202765},
			zoom: 16
		});

		var marker = new google.maps.Marker({
			position: {lat: 38.659784, lng: -9.202765},
			map: map
		});

		var contentString = '<div id="content">'+
		'<h1 id="title">116-II</h1>'+
		'<p>A Sala 116-II é a sala onde decorrem as sessões de formação de APDC PEI</p>' +
		'<div id="media">'+
		'<img src="media/img/DSC_0001.JPG">'+
		'</div>';

		var infowindow = new google.maps.InfoWindow({content: contentString});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});
	} 

	google.maps.event.addDomListener(window, 'load', $scope.initialize);   

	$scope.makeMap = function(user){
		map = new google.maps.Map(document.getElementById('map_div'), {
			center: {lat: 38.659784, lng: -9.202765},
			zoom: 16
		});

		var marker = new google.maps.Marker({
			position: {lat: 38.659784, lng: -9.202765},
			map: map
		});

		var contentString = '<div id="content">'+
		'<h1 id="title">116-II</h1>'+
		'<p>A Sala 116-II é a sala onde decorrem as sessões de formação de APDC PEI</p>' +
		'<div id="media">'+
		'<img src="media/img/DSC_0001.JPG">'+
		'</div>';

		var infowindow = new google.maps.InfoWindow({content: contentString});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});

	}

}]);

//Limit numbers
app.directive("limitTo", [function() {
	return {
		restrict: "A",
		link: function(scope, elem, attrs) {
			var limit = parseInt(attrs.limitTo);
			angular.element(elem).on("keypress", function(e) {
				if (this.value.length == limit) e.preventDefault();
			});
		}
	}
}]);

//Confirm password
app.directive("passwordVerify", function() {
	return {
		require: "ngModel",
		scope: {
			passwordVerify: '='
		},
		link: function(scope, element, attrs, ctrl) {
			scope.$watch(function() {
				var combined;

				if (scope.passwordVerify || ctrl.$viewValue) {
					combined = scope.passwordVerify + '_' + ctrl.$viewValue;
				}
				return combined;
			}, function(value) {
				if (value) {
					ctrl.$parsers.unshift(function(viewValue) {
						var origin = scope.passwordVerify;
						if (origin !== viewValue) {
							ctrl.$setValidity("passwordVerify", false);
							return undefined;
						} else {
							ctrl.$setValidity("passwordVerify", true);
							return viewValue;
						}
					});
				}
			});
		}
	};
});
