package util;

public class RegisterData {
	public String username;
	public String name;
	public String email;
	public String mobileNumber;
	public String telephoneNumber;
	public String nif;
	public String cc;
	public String street;
	public String addressExtras;
	public String city;
	public String zipCode;
	public String password;
	public String confirmation;
	public String lat;
	public String lng;
	
	
	public RegisterData(){}
	
	public RegisterData(String username,String name,String email,String mobileNumber,String telephoneNumber,String nif,String cc,String password,String confirmation,String street,String city,String zipCode,String lat , String lng){
		this.username= username;
		this.name= name;
		this.email= email;
		this.mobileNumber = mobileNumber;
		this.telephoneNumber = telephoneNumber;
		this.nif= nif;
		this.cc= cc;
		this.street= street;
		this.city= city;
		this.zipCode= zipCode;
		this.password= password;
		this.confirmation= confirmation;
		this.lat= lat;
		this.lng=lng;
	}
	
	public RegisterData(String username,String name,String email,String mobileNumber,String telephoneNumber,String nif,String cc,String password,String confirmation,String street,String city,String zipCode,String addressExtras,String lat , String lng){
		this.username= username;
		this.name= name;
		this.email= email;
		this.mobileNumber = mobileNumber;
		this.telephoneNumber = telephoneNumber;
		this.nif= nif;
		this.cc= cc;
		this.street= street;
		this.addressExtras= addressExtras;
		this.city= city;
		this.zipCode= zipCode;
		this.password= password;
		this.confirmation= confirmation;
		this.lat= lat;
		this.lng=lng;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals(""); //verification should be done like in frontEnd
	}
	
	public boolean validRegistration() {
		return validField(username) &&
			   validField(name) &&
			   validField(email) &&
			   validField(mobileNumber) && 
			   validField(nif) &&
			   validField(cc) &&
			   validField(street) &&
			   validField(zipCode)&&
			   validField(password) &&
			   validField(confirmation) &&
			   password.equals(confirmation) &&
			   email.contains("@");		
	}


}
