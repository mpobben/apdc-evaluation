package util;

public class UserMapData {
	public String lat;
	public String lng;
	public String username;
	public String street;
	public String city;
	public String zipCode;
	
	public UserMapData(String username, String street, String zipCode, String city ,String lat,String lng){
		this.lat=lat;
		this.lng=lng;
		this.street=street;
		this.city = city;
		this.zipCode = zipCode;
		this.username= username;
	}
	

}
