package util;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;

public class AuthToken {
	public String username;
	public String tokenId;
	public Long beginDate;
	public Long expireDate;
	
	public AuthToken(){}
	
	public AuthToken(String username){
		this.username= username;
		this.tokenId =generateToken();
		setDate();
		
	}
	
	private void setDate(){
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		date.setTime(System.currentTimeMillis());
		calendar.setTime(date);
		this.beginDate = date.getTime();
		calendar.add(Calendar.HOUR, 1);
		this.expireDate = calendar.getTimeInMillis();
	}

	private String generateToken() {
		SecureRandom random = new SecureRandom();
		Long num = random.nextLong()*System.currentTimeMillis();
		return DigestUtils.shaHex(num.toString())+"-"+System.currentTimeMillis();
	}
	
	public boolean isValid(){
		return System.currentTimeMillis() < this.expireDate;
	}

}
