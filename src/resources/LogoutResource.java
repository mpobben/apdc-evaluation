package resources;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import util.AuthToken;

@Path("/logout")
@Consumes(MediaType.APPLICATION_JSON+ ";charset=utf-8")
public class LogoutResource {
	
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();


	public LogoutResource(){}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogout(AuthToken token, 
			@Context HttpServletRequest request,
			@Context HttpHeaders headers) {
		LOG.fine("Attempt to logout user: " + token.username);

		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", token.username);
		try {
			Entity user = datastore.get(userKey);
			LOG.info("username:"+token.username);

			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey() );
				ustats.setProperty("user_stats_logins", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}

			// Construct the logs
				Entity log = new Entity("UserLog", user.getKey());
				log.setProperty("user_logout_ip", request.getRemoteAddr());
				log.setProperty("user_logout_host", request.getRemoteHost());
				log.setProperty("user_logout_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
				log.setProperty("user_logout_city", headers.getHeaderString("X-AppEngine-City"));
				log.setProperty("user_logout_country", headers.getHeaderString("X-AppEngine-Country"));
				log.setProperty("user_logout_time", new Date());
				// Get the user statistics and updates it
				ustats.setProperty("user_stats_logouts", 1L + (long) ustats.getProperty("user_stats_logouts"));
				ustats.setProperty("user_stats_failed", 0L );
				ustats.setProperty("user_stats_last", new Date());		

				//Deletes Tokens 
				Query tokenQ = new Query("UserTokens").setAncestor(user.getKey()).setKeysOnly();
				List<Entity> tokens = datastore.prepare(txn,tokenQ).asList(FetchOptions.Builder.withDefaults());
				for(Entity e : tokens)
					datastore.delete(e.getKey());
				
				// Batch operation
				List<Entity> logs = Arrays.asList(log,ustats);
				datastore.put(txn,logs);
				txn.commit();
				
				return Response.ok().build();				
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed logout attempt for username: " + token.username);
			LOG.warning("Check log if printed" + token.username);
			return Response.status(Status.FORBIDDEN).build();
		} catch (Exception e){
			LOG.warning("Exception thrown:"+e.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}


}
