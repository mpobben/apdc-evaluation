package resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.api.client.util.store.DataStore;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import util.AuthToken;
import util.LoginData;
import util.UserMapData;

@Path("/info")
public class UserInfoResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();

	public UserInfoResource(){}

	/**
	 * gets user info to populate map
	 */
	@POST
	@Path("/user/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getUserByUsername(@PathParam("id") String username, AuthToken token,@Context HttpServletRequest request,
			@Context HttpHeaders headers){
		LOG.fine("Attempt to get all usersInfo username: " + token.username +" token username:"+ token.username);

		Transaction txn = datastore.beginTransaction();
		Key userKeyLog = KeyFactory.createKey("User", token.username);
		Key userKeyMapInfo = KeyFactory.createKey("User", username);
		
		try {
			Entity user = datastore.get(userKeyLog);
			
			LOG.info("username:"+token.username);


			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKeyLog);
			List<Entity> results = datastore.prepare(txn,ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey() );
				ustats.setProperty("user_stats_get_info_all", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}

			// Construct the logs
			Entity log = new Entity("UserLog", user.getKey());
			log.setProperty("user_infoResource_ip", request.getRemoteAddr());
			log.setProperty("user_infoResource_host", request.getRemoteHost());
			log.setProperty("user_infoResource_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
			log.setProperty("user_infoResource_city", headers.getHeaderString("X-AppEngine-City"));
			log.setProperty("user_infoResource_country", headers.getHeaderString("X-AppEngine-Country"));
			log.setProperty("user_infoResource_time", new Date());	

			System.out.println("after logging");
			Query userTokenQ = new Query("UserTokens").setAncestor(userKeyMapInfo);

			List<Entity> tokens = datastore.prepare(txn,userTokenQ).asList(FetchOptions.Builder.withDefaults());
			System.out.println("after getting tokkens");

			if(tokenIsValid(token, tokens)){ 
				// Batch operation
				List<Entity> logs = Arrays.asList(log,ustats);
				datastore.put(txn,logs);

				Filter propertyFilter = new FilterPredicate("user_username",FilterOperator.EQUAL,username);
				
				Query usersQ = new Query("User").setFilter(propertyFilter);
				System.out.println("before getting users");
				List<Entity> users = datastore.prepare(txn,usersQ).asList(FetchOptions.Builder.withDefaults());
				System.out.println("after getting users");
				
				Query addressQ ;
				Entity addressEntity;
				List<UserMapData> gMapData = new LinkedList<UserMapData>(); 
				for(Entity userEntity : users ){
					addressQ = new Query("Address").setAncestor(user.getKey());
					addressEntity = datastore.prepare(txn,addressQ).asList(FetchOptions.Builder.withDefaults()).get(0);
					gMapData.add(new UserMapData(userEntity.getProperty("user_username").toString(),addressEntity.getProperty("user_street").toString(),addressEntity.getProperty("user_zipCode").toString(),addressEntity.getProperty("user_city").toString(),addressEntity.getProperty("lat").toString(),addressEntity.getProperty("lng").toString()));
					System.out.println("after creating usermapdata");
				}
				
				txn.commit();

				// Return token
				LOG.info("User '" + token.username + "' got map info sucessfully.");
				System.out.println();
				return Response.ok(g.toJson(gMapData)).build();				
			} else {
				// Incorrect password
				ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
				List<Entity> logs = Arrays.asList(log,ustats);
				datastore.put(txn,logs);				
				txn.commit();

				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Invalid token for username: " + token.username);
			return Response.status(Status.FORBIDDEN).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}

	/**
	 * gets all users info to populate map
	 */
	@POST
	@Path("/allusers/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAllUsers(AuthToken token, @Context HttpServletRequest request,
			@Context HttpHeaders headers){

		LOG.fine("Attempt to get all usersInfo username: " + token.username +" token username:"+ token.username);

		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", token.username);
		try {
			Entity user = datastore.get(userKey);
			
			LOG.info("username:"+token.username);


			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey() );
				ustats.setProperty("user_stats_get_info_all", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}

			// Construct the logs
			Entity log = new Entity("UserLog", user.getKey());
			log.setProperty("user_infoResource_ip", request.getRemoteAddr());
			log.setProperty("user_infoResource_host", request.getRemoteHost());
			log.setProperty("user_infoResource_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
			log.setProperty("user_infoResource_city", headers.getHeaderString("X-AppEngine-City"));
			log.setProperty("user_infoResource_country", headers.getHeaderString("X-AppEngine-Country"));
			log.setProperty("user_infoResource_time", new Date());	

			System.out.println("after logging");
			Query userTokenQ = new Query("UserTokens").setAncestor(userKey);

			List<Entity> tokens = datastore.prepare(txn,userTokenQ).asList(FetchOptions.Builder.withDefaults());
			System.out.println("after getting tokkens");

			if(tokenIsValid(token, tokens)){ 
				// Batch operation
				List<Entity> logs = Arrays.asList(log,ustats);
				datastore.put(txn,logs);

				Filter propertyFilter = new FilterPredicate("is_root",FilterOperator.EQUAL,"true");
				
				Query usersQ = new Query("User").setFilter(propertyFilter);
				System.out.println("before getting users");
				List<Entity> users = datastore.prepare(usersQ).asList(FetchOptions.Builder.withDefaults());
				System.out.println("after getting users");
				
				Query addressQ ;
				Entity addressEntity;
				List<UserMapData> gMapData = new LinkedList<UserMapData>(); 
				for(Entity userEntity : users ){
					addressQ = new Query("Address").setAncestor(user.getKey());
					addressEntity = datastore.prepare(txn,addressQ).asList(FetchOptions.Builder.withDefaults()).get(0);
					gMapData.add(new UserMapData(userEntity.getProperty("user_username").toString(),addressEntity.getProperty("user_street").toString(),addressEntity.getProperty("user_zipCode").toString(),addressEntity.getProperty("user_city").toString(),addressEntity.getProperty("lat").toString(),addressEntity.getProperty("lng").toString()));
					System.out.println("after creating usermapdata");
				}
				
				txn.commit();

				// Return token
				LOG.info("User '" + token.username + "' got map info sucessfully.");
				System.out.println();
				return Response.ok(g.toJson(gMapData)).build();				
			} else {
				// Incorrect password
				ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
				List<Entity> logs = Arrays.asList(log,ustats);
				datastore.put(txn,logs);				
				txn.commit();

				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Invalid token for username: " + token.username);
			return Response.status(Status.FORBIDDEN).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}

	private boolean tokenIsValid(AuthToken token,List<Entity> dbTokens){
		for(Entity e : dbTokens){
			if(e.getProperty("token_id").toString().equals(token.tokenId) && e.getProperty("expire_date").toString().equals(token.expireDate.toString()) && token.expireDate > System.currentTimeMillis())
				return true;
		}
		return false;

	}

}
