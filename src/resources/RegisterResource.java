package resources;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import util.RegisterData;

@Path("/register")
@Consumes(MediaType.APPLICATION_JSON+ ";charset=utf-8")
public class RegisterResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public RegisterResource(){}

	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doRegistration(RegisterData data) {

		if( ! data.validRegistration() ) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}

		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.username);
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("User", data.username);
			Entity address = new Entity("Address", user.getKey());
			user.setProperty("user_username", data.username);
			user.setProperty("is_root", "true");
			user.setProperty("user_name", data.name);
			user.setProperty("user_pwd", DigestUtils.shaHex(data.password));
			user.setProperty("user_email", data.email);
			user.setProperty("user_nif", data.nif);
			user.setProperty("user_cc", data.cc);
			user.setProperty("user_mobileNumber", data.mobileNumber);
			user.setProperty("user_telephoneNumber", data.telephoneNumber);
			address.setProperty("user_zipCode", data.zipCode);
			address.setProperty("user_street", data.street);
			address.setProperty("user_city", data.city);
			address.setProperty("lat", data.lat);
			address.setProperty("lng", data.lng);
			user.setUnindexedProperty("user_creation_time", new Date());
			address.setUnindexedProperty("user_creation_time", new Date());
			List<Entity> userInfo = Arrays.asList(user,address);
			datastore.put(txn,userInfo);
			LOG.info("User registered " + data.username);
			txn.commit();
			return Response.ok().build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}



}
